from django.urls import path
from rest_framework.routers import DefaultRouter

from core.api_views import (
    ConnexionMedecinAPIView,
    DossierMedicalViewSet,
    ExamenViewSet,
    MedecinViewSet,
    ObservationViewSet,
    ObtenirDossierMedicalAPIView,
    OrdonnanceViewSet
)


router = DefaultRouter()
router.register('doctor_user', MedecinViewSet, basename='doctor_user')
router.register('observations', ObservationViewSet, basename='observations')
router.register('ordonnances', OrdonnanceViewSet, basename='ordonnances')
router.register('examens', ExamenViewSet, basename='examens')
router.register('medical_folder', DossierMedicalViewSet, basename='medical_folder')

urlpatterns = [
    # path('medical_folder/', DossierMedicalAPIView.as_view()),
    path('get_medical_folder/', ObtenirDossierMedicalAPIView.as_view()),
    path('doctor/connection/', ConnexionMedecinAPIView.as_view()),
]

urlpatterns += router.urls
