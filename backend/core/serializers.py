from rest_framework import serializers

from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from core.models import DossierMedical, Examen, Medecin, Observation, Ordonnance
from core.utils import generate_code


class DossierMedicalSerializer(serializers.ModelSerializer):
    code = serializers.ReadOnlyField()
    date_creation = serializers.ReadOnlyField()
    
    class Meta:
        model = DossierMedical
        fields = '__all__'
    
    def create(self, validated_data):
        code = ''
        while True:
            code = generate_code()
            if DossierMedical.objects.filter(code=code).exists():
                code = generate_code()
                
            else:
                break
        
        return DossierMedical.objects.create(
            code=code,
            nom=validated_data['nom'].upper(),
            prenom=validated_data['prenom'].upper(),
            date_de_naissance=validated_data['date_de_naissance'],
            lieu_de_naissance=validated_data['lieu_de_naissance'].capitalize(),
            poids=validated_data['poids'],
            taille=validated_data['taille'],
            sexe=validated_data['sexe'],
            profession=validated_data['profession'].capitalize(),
            numero_cni=validated_data['numero_cni'],
            telephone=validated_data['telephone'],
            status=validated_data['status'],
            cree_par=validated_data['cree_par']
        )


class ObtenirDossierMedicalSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=100)

    def validate(self, attrs):
        code = attrs.get('code')
        if len(code) != 29:
            attrs['error'] = "Le code doit contenir 29 caractères."
        
        if len(code.split('-')) != 5:
            attrs['error'] = "Le code n'est pas valide."
            
        return attrs


class UtilisateurSerializer(serializers.ModelSerializer):
    username = serializers.EmailField()
    
    class Meta:
        model = User
        fields = ('first_name', 'username', 'password')


class IUtilisateurSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('first_name', 'username')


class MedecinSerializer(serializers.ModelSerializer):
    utilisateur = UtilisateurSerializer()
    
    class Meta:
        model = Medecin
        fields = ('id', 'utilisateur', 'specialite')
        
    def validate(self, attrs):
        nouveau = attrs['utilisateur']
        username_propose = nouveau['username']
        
        # on vérifie s'il existe déjà un médecin avec cette adresse mail
        if Medecin.objects.filter(utilisateur__username=username_propose).exists():
            raise serializers.ValidationError("Un médecin avec une telle adresse existe déjà.")
        
        return attrs
    
    def create(self, validated_data):
        donnees_utilisateur = validated_data.pop('utilisateur')
        nom = donnees_utilisateur['first_name']
        nom_utilisateur = donnees_utilisateur['username']
        mot_de_passe = donnees_utilisateur['password']
        utilisateur = User.objects.create_user(
            username=nom_utilisateur,
            password=mot_de_passe,
            first_name=nom,
        )
        
        return Medecin.objects.create(
            utilisateur=utilisateur,
            specialite=validated_data['specialite']
        )
        

class IMedecinSerializer(serializers.ModelSerializer):
    utilisateur = IUtilisateurSerializer()
    
    class Meta:
        model = Medecin
        fields = '__all__'


class ObservationSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()
    
    class Meta:
        model = Observation
        fields = '__all__'
        

class IObservationSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()
    redigee_par = IMedecinSerializer()
    
    class Meta:
        model = Observation
        fields = '__all__'


class OrdonnanceSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()
    
    class Meta:
        model = Ordonnance
        fields = '__all__'


class IOrdonnanceSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()
    prescrite_par = IMedecinSerializer()
    
    class Meta:
        model = Ordonnance
        fields = '__all__'
        

class ExamenSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()
    
    class Meta:
        model = Examen
        fields = '__all__'
        

class IExamenSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()
    demande_par = IMedecinSerializer()
    
    class Meta:
        model = Examen
        fields = '__all__'
        

class ConnexionMedecinSerializer(serializers.ModelSerializer):
    username = serializers.EmailField()
    
    class Meta:
        model = User
        fields = ('username', 'password')
    
    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        
        if not User.objects.filter(username=username).exists():
            attrs['error'] = "L'adresse mail n'existe pas."
            return attrs
        
        if authenticate(username=username, password=password) is None:
            attrs['error'] = "Le mot de passe est incorrect."
            return attrs
    
        return attrs
