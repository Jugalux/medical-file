from django.db import models
from django.contrib.auth.models import User


SEXE = [
    ('M', 'Masculin'),
    ('F', 'Féminin')
]

STATUS = [
    ('Publique', 'Publique'),
    ('Privé', 'Privé')
]


class DossierMedical(models.Model):
    """Classe représentant le dossier médical d'un patient."""
    
    code = models.CharField(max_length=100, null=False, unique=True)
    nom = models.CharField(max_length=255, null=False)
    prenom = models.CharField(max_length=255, null=False)
    date_de_naissance = models.DateField(null=False)
    lieu_de_naissance = models.CharField(max_length=255, null=False)
    poids = models.FloatField(null=False)
    taille = models.FloatField(null=False)
    sexe = models.CharField(max_length=10, choices=SEXE, null=False)
    photo = models.ImageField(null=True, blank=True)
    profession = models.CharField(max_length=255, null=True)
    numero_cni = models.CharField(max_length=255, null=True)
    telephone = models.CharField(max_length=13, null=True)
    status = models.CharField(max_length=10, choices=STATUS, null=False, default='Publique')
    date_creation = models.DateTimeField(auto_now_add=True)
    cree_par = models.ForeignKey('Medecin', on_delete=models.PROTECT, default=None)
    
    def __str__(self):
        return self.code
    

class Observation(models.Model):
    """Classe représentant une observation sur un patient."""
    
    dossier_medical = models.ForeignKey(DossierMedical, on_delete=models.CASCADE, null=False)
    date = models.DateTimeField(auto_now=True)
    observation = models.TextField(null=False, blank=False)
    redigee_par = models.ForeignKey('Medecin', on_delete=models.PROTECT)
    
    def __str__(self):
        return str(self.date)
    

class Ordonnance(models.Model):
    """Classe représentant une ordonnance pour un patient."""
    
    dossier_medical = models.ForeignKey(DossierMedical, on_delete=models.CASCADE, null=False)
    date = models.DateTimeField(auto_now=True)
    ordonnance = models.TextField(null=False, blank=False)
    prescrite_par = models.ForeignKey('Medecin', on_delete=models.PROTECT)
    
    def __str__(self):
        return str(self.date)
    

# Il faut réfléchir à une facon comment lier les resultats d'un examen avec l'examen    
class Examen(models.Model):
    """Classe représentant un examen à donner à un patient."""
    
    dossier_medical = models.ForeignKey(DossierMedical, on_delete=models.CASCADE, null=False)
    date = models.DateTimeField(auto_now=True)
    examen = models.TextField(null=False, blank=False)
    resultat = models.FileField(null=True, blank=True)
    demande_par = models.ForeignKey('Medecin', on_delete=models.PROTECT)
    
    def __str__(self):
        return str(self.date)


class Medecin(models.Model):
    """Classe représentant un médécin de la plateforme"""
    
    utilisateur = models.OneToOneField(User, on_delete=models.CASCADE)
    specialite = models.CharField(max_length=255, null=False, blank=False)
    
    """D'autre champ seront ajouter en cas de besoin"""
    
    def __str__(self):
        return self.utilisateur.username
