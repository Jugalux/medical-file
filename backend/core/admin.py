from django.contrib import admin

from core.models import DossierMedical, Examen, Medecin, Observation, Ordonnance


@admin.register(Medecin)
class MedecinAdmin(admin.ModelAdmin):
    list_display = ('utilisateur', 'specialite')
    search_fields = ('utilisateur__firstname', 'utilisateur__username')
    

@admin.register(DossierMedical)
class DossierMedicalAdmin(admin.ModelAdmin):
    list_display = ('code', 'status', 'date_creation', 'cree_par')
    list_filter = ('status', 'cree_par', 'sexe')
    search_fields = ('code', 'nom', 'prenom', 'telephone')
    

@admin.register(Observation)
class ObservationAdmin(admin.ModelAdmin):
    list_display = ('dossier_medical', 'date', 'redigee_par')
    list_filter = ('dossier_medical', 'redigee_par')
    

@admin.register(Ordonnance)
class OrdonnanceAdmin(admin.ModelAdmin):
    list_display = ('dossier_medical', 'date', 'prescrite_par')
    list_filter = ('dossier_medical', 'prescrite_par')


@admin.register(Examen)
class ExamenAdmin(admin.ModelAdmin):
    list_display = ('dossier_medical', 'date', 'demande_par')
    list_filter = ('dossier_medical', 'demande_par')

