from django.contrib.auth.models import User

from rest_framework.response import Response
from rest_framework import generics, viewsets
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin, UpdateModelMixin

from core.models import DossierMedical, Examen, Medecin, Observation, Ordonnance
from core.serializers import (
    ConnexionMedecinSerializer, 
    DossierMedicalSerializer, 
    ExamenSerializer, 
    IExamenSerializer, 
    IMedecinSerializer, 
    IObservationSerializer, 
    IOrdonnanceSerializer, 
    MedecinSerializer, 
    ObservationSerializer, 
    ObtenirDossierMedicalSerializer, 
    OrdonnanceSerializer,
)


# class DossierMedicalAPIView(generics.GenericAPIView):
#     serializer_class = DossierMedicalSerializer
#     queryset = DossierMedical.objects.all()
    
#     def post(self, request):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
        
#         code = ''
#         while True:
#             code = generate_code()
#             if DossierMedical.objects.filter(code=code).exists():
#                 code = generate_code()
                
#             else:
#                 break
        
#         dossier = DossierMedical.objects.create(
#             code=code,
#             nom=serializer.validated_data['nom'].capitalize(),
#             prenom=serializer.validated_data['prenom'].capitalize(),
#             date_de_naissance=serializer.validated_data['date_de_naissance'],
#             lieu_de_naissance=serializer.validated_data['lieu_de_naissance'],
#             poids=serializer.validated_data['poids'],
#             taille=serializer.validated_data['taille'],
#             sexe=serializer.validated_data['sexe'],
#             profession=serializer.validated_data['profession'],
#             numero_cni=serializer.validated_data['numero_cni'],
#             telephone=serializer.validated_data['telephone'],
#             status=serializer.validated_data['status'],
#             cree_par=serializer.validated_data['cree_par']
#         )
        
#         if dossier:
#             return Response("Dossier créé.", status=status.HTTP_201_CREATED)
        
#         else:
#             return Response("Dossier non créé.", status=status.HTTP_400_BAD_REQUEST)
    
#     def get(self, request):
#         queryset = self.get_queryset()
#         serializer = self.get_serializer(queryset, many=True)
        
#         return Response(serializer.data)

class DossierMedicalViewSet(
    CreateModelMixin,
    RetrieveModelMixin, 
    ListModelMixin,
    UpdateModelMixin,
    viewsets.GenericViewSet):

    serializer_class = DossierMedicalSerializer
    queryset = DossierMedical.objects.all()


class ObtenirDossierMedicalAPIView(generics.GenericAPIView):
    serializer_class = ObtenirDossierMedicalSerializer
    
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        if serializer.validated_data.get('error'):
            return Response(serializer.validated_data['error'])
        
        dossier = DossierMedical.objects.get(code=serializer.validated_data['code'])
        if dossier:
            return Response(DossierMedicalSerializer(dossier).data)
        
        else:
            return Response('Dossier non trouvé')


class MedecinViewSet(
    CreateModelMixin, 
    RetrieveModelMixin,
    ListModelMixin,
    UpdateModelMixin, 
    viewsets.GenericViewSet):
    
    serializer_class = MedecinSerializer
    queryset = Medecin.objects.all()
    
    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return IMedecinSerializer
        
        return super().get_serializer_class()
    
    
class ObservationViewSet(
    CreateModelMixin, 
    # RetrieveModelMixin,
    ListModelMixin, 
    viewsets.GenericViewSet):
    
    serializer_class = ObservationSerializer
    queryset = Observation.objects.all()
    
    def get_serializer_class(self):
        if self.action == 'list':
            return IObservationSerializer
        
        return super().get_serializer_class()
    

class OrdonnanceViewSet(
    CreateModelMixin,
    # RetrieveModelMixin,
    ListModelMixin,
    viewsets.GenericViewSet):
    
    serializer_class = OrdonnanceSerializer
    queryset = Ordonnance.objects.all()
    
    def get_serializer_class(self):
        if self.action == 'list':
            return IOrdonnanceSerializer
        
        return super().get_serializer_class()
    

class ExamenViewSet(
    CreateModelMixin,
    # RetrieveModelMixin,
    ListModelMixin,
    viewsets.GenericViewSet):
    
    serializer_class = ExamenSerializer
    queryset = Examen.objects.all()
    
    def get_serializer_class(self):
        if self.action == 'list':
            return IExamenSerializer
        
        return super().get_serializer_class()
    

class ConnexionMedecinAPIView(generics.GenericAPIView):
    serializer_class = ConnexionMedecinSerializer
    
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        if serializer.validated_data.get('error'):
            return Response(serializer.validated_data['error'])
        
        utilisateur = User.objects.get(
            username=serializer.validated_data['username'])
        
        medecin = Medecin.objects.get(utilisateur=utilisateur)
        if medecin:
            return Response(IMedecinSerializer(medecin).data)
        
        else:
            return Response('Medecin non trouvé')
