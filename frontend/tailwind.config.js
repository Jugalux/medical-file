/**@type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    extend: { 
      backgroundImage: {
        'projectBackground': "url('./media/gradient.png')",
      },
      colors:{
        'primary':'#003570',
        'secondary':'#FFE9E4',
        'danger': '#e24065'
      },
      fontFamily: {
        'Poppins-Regular': ['Poppins-Regular', 'sans-serif'],
      },
    },
  },
  plugins: [],
}
