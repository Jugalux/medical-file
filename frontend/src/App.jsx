import React from "react";
import { BrowserRouter } from "react-router-dom";
import Router from "./Router";
import { MedicalFolderContextProvider } from "./services/MedicalFolder";

const App = () => {
  return (
    <MedicalFolderContextProvider>
      <BrowserRouter>
        <Router />
      </BrowserRouter>
    </MedicalFolderContextProvider>
  );
};

export default App;
