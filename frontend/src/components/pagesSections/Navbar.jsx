import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { IoIosAddCircle } from 'react-icons/io'
import { Modal } from "@mui/material"
import ModalPopup from '../pagesSections/CreateMedicalFilePopup'
import ModalLogin from './ModalLogin'
import logo from '../../media/logo.png'
import { useNavigate } from 'react-router'


function Navbar({ page }) {
    let navigate = useNavigate()

    const [openModal, setOpenModal] = useState(false)
    const handleClose = () => setOpenModal(false)
    const handleOpen = () => setOpenModal(true)

    const connexion = () => {
        return <>
            <div className='my-auto'>
                {window.localStorage.getItem('userId') ?
                     <div onClick={() => navigate("/search")} className="flex justify-center px-5 py-2 bg-green-400 text-white border rounded-full hover:cursor-pointer">
                     <div>
                         <p>Gestion de dossier</p>
                     </div>
                 </div> :
                        <div onClick={() => navigate("/login")} className="flex justify-center px-5 py-2 bg-primary text-white border rounded-full hover:cursor-pointer">
                            <div>
                                <p>Connexion</p>
                            </div>
                        </div>
                    }
            </div>
            <div className="min-h-full">
                <Modal
                    open={openModal}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                    sx={{ overflowY: "scroll" }}
                >
                    <div className="w-full">
                        <ModalLogin close={handleClose} />
                    </div>
                </Modal>
            </div>
        </>
    }

    const addMedicalFile = () => {
        return <>
            <div className='my-auto'>
                <div onClick={() => handleOpen()} className="flex px-4 py-2 bg-primary text-white border rounded-full hover:cursor-pointer">
                    <IoIosAddCircle color='white' size={24} className='mr-2' />
                    <p>Ajouter un dossier</p>
                </div>
            </div>
            <div className="min-h-full">
                <Modal
                    open={openModal}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                    sx={{ overflowY: "scroll" }}
                >
                    <div className="w-full">
                        <ModalPopup close={handleClose} />
                    </div>
                </Modal>
            </div>
        </>
    }

    const createFile = () => {
        return (
            <Link to='/' className='my-auto' onClick={()=> {window.localStorage.clear()}} >
                <p className='text-red-500 underline'>Deconnexion</p>
            </Link>
        )
    }

    return (
        <div className='sticky top-0 z-30 w-full'>
            <div className='flex justify-between p-1 bg-danger lg:px-20 px-3 text-white'>
                <small>Centre d'aide: www.aide.com</small>
                <small>{JSON.parse(window.localStorage.getItem('data'))?.utilisateur?.first_name || ''}</small>
                <small className=''> {JSON.parse(window.localStorage.getItem('data'))?.specialite || ''}
                    {/* Tel: +237 654 371 312 */}
                    </small>
            </div>
            <div className='flex justify-between bg-gray-50 lg:px-20 px-3'>
                <Link to='/'>
                    <div className='flex'>
                        <img src={logo} alt="" width={70} height={70} />
                        <p className='text-blue-900 font-bold my-auto'>Dossier Medical</p>
                    </div>
                </Link>
                <div className='flex'>
                    {
                        page === 'search' ?
                            addMedicalFile()
                            : null
                    }
                    {
                        page === 'home' ?
                            connexion()
                            : null
                    }
                    {
                        page === 'file' ?
                            createFile()
                            : null
                    }
                </div>
            </div>
        </div>
    )
}

export default Navbar