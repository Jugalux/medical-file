import React from 'react'
import { Link } from 'react-router-dom'
import avatar from '../../../media/avatar.png'

function UserBox({name, surname, sex, id}) {

    return (
        
        <div className="max-w-sm h-full bg-white border border-gray-200 rounded-lg shadow-md">
            <div className='flex justify-center py-5 rounded-lg' style={{ backgroundColor: 'rgba(0,53,112,.19)' }}>
                <img class="rounded-t-lg" src={avatar} alt="" width={200} height={200} />
            </div>
            <div className="p-5 content-between">
                <p className="mb-2 font-normal text-gray-700 dark:text-gray-400">Nom : 
                    <span className='font-bold text-gray-800'> {name}</span>
                </p>
                <p className="mb-2 font-normal text-gray-700 dark:text-gray-400">Prenom : 
                    <span className='font-bold text-gray-800'> {surname}</span>
                </p>
                <p className="mb-2 font-normal text-gray-700 dark:text-gray-400">Sexe : 
                    <span className='font-bold text-gray-800'> {sex}</span>
                </p>
                <div className='flex justify-end'>
                    <Link to={`/file/${id}`} className="inline-flex items-center px-5 py-2 text-sm font-medium text-center text-white bg-blue-900 rounded-lg hover:bg-blue-800 transition focus:ring-1 focus:outline-none focus:ring-blue-900">
                        Détails
                        <svg aria-hidden="true" className="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    </Link>
                </div>
            </div>
        </div>

    )
}

export default UserBox