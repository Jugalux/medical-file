import React, { useState, useEffect } from "react"
import { ImCross } from "react-icons/im"
import { createSearchParams, useNavigate } from "react-router-dom"
import axios from '../api/axios'
import Title from "../elements/Title"
import Input from "../elements/Input"
import Button from "../elements/Button"
import MedicalFolder from '../../pages/medicalFolder/MedicalFolder'


const ModalAddFile = ({ close }) => {

	const [code, setCode] = useState('')
	const [data, setData] = useState('')
	const [currentData, setCurrentData] = useState([])
	const [isValidCode, setIsValidCode] = useState()

	const navigate = useNavigate()

	useEffect(() => {
		getData()
	})

	const getData = async () => {
		const response = await axios.get('medical_folder')
		setData(response.data)
	}

	const goToMedicalFile = () => {

		//check if code exist in DataBase, then redirect to file route with data parameters
		for(let obj in data){
			if(code == data[obj].code){
				setCurrentData(data[obj])
				setIsValidCode(true)
				//navigate(`file/${currentData.id}`, { state: { aze } })
				navigate({
					pathname: `file/${data[obj].id}`,
					search: createSearchParams({
						id: data[obj].id,
						//code: currentData.code,
						// data: currentData
					}).toString()
				})
			}else {
				setIsValidCode(false)
			}
		}		
	}

	return (
		<section className="bg-transparent w-full min-h-full flex justify-center items-center ">
			<div className="lg:w-[50vw] md:w-[70vw] w-[100vw] bg-white py-4 px-5 relative m-4 rounded-sm ">
				<ImCross
					className="text-primary absolute right-3 top-3 hover:cursor-pointer hover:text-blue-800"
					onClick={() => close()}
				/>
				<Title
					titleName="Accéder à mon dossier"
					subtitleName="Veuillez entrer vode code d’identification pour accéder à votre dossier patiente"
				/>
				<div className="justify-center items-center mb-14 mt-5">
                    <div className="w-full">
                        <Input
                            type='text'
                            name='Code'
                            varName='code'
							placeholder='XXX - XXX - XXX - XXX - XXX'
                            value={code}
                            handleChange={(e) => setCode(e.target.value)}
                        />
						{
							isValidCode === false ? 
								<small className='text-red-500'>Veuillez entrer un code valide pour accéder à votre dossier</small>
							:
								null
						}
                    </div>
				</div>
				<Button
					name="Consulter mon dossier"
					color='#e24065'
					filled={true}
					handleClick={goToMedicalFile}
				/>
			</div>
		</section>
	)
}

export default ModalAddFile
