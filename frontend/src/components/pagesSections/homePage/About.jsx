import React from 'react'


function About() {


    return (
        <div class="lg:px-20 md:px-6 px-4 md:py-12 py-8">
            <div class="lg:flex items-center justify-between">
                <div class="lg:w-2/3 lg:mr-8">
                    <h4 className='text-lg text-danger'>A Propos</h4>
					<h3 className='text-3xl text-primary font-bold mb-8'>Pourquoi faire créer son dossier ?</h3>
                    
                    <p className='text-gray-800'>
                        Le dossier d’un patient représente la mémoire intégrale et écrite de son passage dans un établissement
                        hospitalier. Dans ce document, vient s’inscrire la trace de tout acte diagnostique, thérapeutique et
                        préventif, ainsi que la réflexion de la relation médecin-malade. C’est un outil de réflexion, de 
                        synthèse, de planification et de traçabilité des soins, voire de recherche et d’enseignement. 
                        C’est aussi un élément de centralisation des actions de tous les intervenants dans le domaine de la santé.
                    </p>
                    
                    <a href='https://docs.google.com/document/d/1eJdMBlp4Vqxtg5efWID4gweokoXNPpxW1WwEEeoIids/edit?usp=sharing' className="focus:ring-2 focus:ring-offset-2 focus:ring-gray-700 focus:outline-none mt-6 md:mt-8 text-base font-semibold leading-none text-blue-800 flex items-center hover:underline">
                        En savoir plus
                        <svg class="ml-2 mt-1 text-blue-800" width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.33325 4H10.6666" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M8 6.66667L10.6667 4" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M8 1.33398L10.6667 4.00065" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </a>
                </div>
                <div class="lg:w-7/12 lg:mt-0 mt-8">
                    <div class="w-full bg-red-200">
                        <img src="https://images.pexels.com/photos/3845686/pexels-photo-3845686.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" class="w-full sm:block hidden" height='300px' />
                    </div>
                    <div class="grid sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 lg:gap-8 gap-6 lg:mt-8 md:mt-6 mt-4">
                        <img src="https://images.pexels.com/photos/1170979/pexels-photo-1170979.jpeg?auto=compress&cs=tinysrgb&w=600" class="w-full" height="300px" alt="" />
                        <img src="https://images.pexels.com/photos/3825541/pexels-photo-3825541.jpeg?auto=compress&cs=tinysrgb&w=600" class="w-full" height="300px" alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About