import React from 'react'
import image from '../../../media/logo.png'

function Services() {


    return (
			<div className="lg:px-20 py-10">
				<div className="grid lg:grid-cols-2 lg:px-0 px-5">
					<div className="flex items-center">
						<div className="">
							<h4 className='text-lg text-danger'>Nos services</h4>
							<h3 className='text-3xl text-primary font-bold mb-8'>Services que nous offrons</h3>
							<p className='text-gray-800'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Nullam placerat rutrum turpis, sit amet hendrerit metus vulputate id. Nulla laoreet 
								maximus feugiat. Nullam rhoncus, orci vitae convallis venenatis Lorem ipsum dolor sit 
								amet, consectetur adipiscing elit. Nullam placerat rutrum turpis, sit amet hendrerit 
								metus.
							</p>
							
						</div>
					</div>
					<div className="grid lg:grid-cols-2">
						<div className="p-4 lg:mt-auto mt-10 my-5 bg-blue-50 hover:bg-blue-100 cursor-pointer border-2 my-auto rounded">
							<div className="service-icon">
								<img src={image} alt="" width={100} />
							</div>
							<div className="service-text">
								<h3 className='text-xl text-primary font-medium'>Consultation de dossier medicaux</h3>
								<p className='font-thin'> Le citoyen pourra avoir accès à son dossier, ainsi qu'à l'historique
								 des consultations (Observations, Ordonnances, Examens, etc ...) qu'il a eu à faire avec les 
								 différents médecins qu'il a rencontré.
								</p>
							</div>
						</div>
						<div className=" margin-top-sb-30">
							<div className="row">
								<div className="p-4 lg:m-3 my-5 bg-red-50 hover:bg-red-100 cursor-pointer border-2 rounded">
									<div className="service-icon">
										<img src={image} alt="" width={100} />
									</div>
									<div className="service-text">
										<h3 className='text-xl text-primary font-medium'>Impression du dossier patient</h3>
										<p className='font-thin'>Le citoyen pourra imprimer les différents résultats d'examen qu'il a passé,
										 ainsi que les ordonnances prescrites.
										</p>
									</div>
								</div>
								<div className="p-4 bg-yellow-50 my-5 hover:bg-yellow-100 cursor-pointer border-2 lg:m-3 rounded">
									<div className="service-icon">
										<img src={image} alt="" width={100} />
									</div>
									<div className="service-text">
										<h3 className='text-xl text-primary font-medium'>Création de dossier medicaux</h3>
										<p className='font-thin'>
											Le médecin pourra créer le dossier d'un citoyen dans le système s'il n'existe pas déjà,
											puis accéder au dossier d'un patient après l'avoir rechercher dans le système, afin de
											remplir les informations lors des consultations.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    )
}

export default Services