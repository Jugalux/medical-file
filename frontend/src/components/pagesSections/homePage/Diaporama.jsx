import React, { useState } from 'react';

const Diaporama = () => {
    const [currentStep, setCurrentStep] = useState(5);

    const stepDescriptions = [
        "Médecine Interne",
        "Cardiologie",
        "Oncologie Médicale",
        "Chirurgie Urologique",
        "Chirurgie Orthopédique",
        "Chirurgie Pédiatrique",
        "Chirurgie Générale",
        "Neurologie",
        "Dermatologie",
        "Pneumologie",
        "Néphrologie",
        "Hépato-Gastro-Entérologie",
        "Anatomie/Patholoqie",
        "Anesthésie/Réanimation",
        "Gynécologie/Obstétrique",
        "Psychiatrie",
        "Pédiatrie",
        "Radiologie",
        "Ophtalmoloqie",
        "Dermatologie",
        "Endocrinologie",
        "Infectiologie",
        "Hématologie clinique",
        "Neurochirurgie",
        "Traumatologie",
    ];

    const nextStep = () => {
        setCurrentStep(currentStep + 5)
    }

    const prevStep = () => {
        setCurrentStep(currentStep - 5)
    }

    return (
        <div className='pb-10 pt-5'>

            <h4 className='text-lg text-danger text-center'>Spécialisations</h4>
			<h3 className='text-3xl text-primary font-bold text-center mb-8'>Nos experts exercent dans les domaines suivants</h3>

            <div className="flex justify-center">

                {currentStep > 0 && (
                    <button className="bg-primary my-auto h-10 px-3 text-white rounded-full hover:bg-blue-900" onClick={prevStep}>
                        Prev
                    </button>
                )}

                {stepDescriptions
                    .slice(currentStep, currentStep + 5)
                    .map((description, index) => (
                        <div className="p-5 w-max m-3 py-10 bg-blue-900 hover:bg-blue-800 rounded cursor-pointer" key={index}>
                            <p className='text-white'>{description}</p>
                        </div>
                    ))
                }

                {currentStep + 5 < stepDescriptions.length && (
                    <button className="bg-primary my-auto h-10 px-3 text-white rounded-full hover:bg-blue-900" onClick={nextStep}>
                        Next
                    </button>
                )}
            </div>
        </div>
    );
};

export default Diaporama;
