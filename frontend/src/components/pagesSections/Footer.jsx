import React from 'react'
import { FaFacebook, FaTwitter, FaYoutube } from 'react-icons/fa'


function Footer() {

    return (
            <footer className="py-8 sm:py-12 text-white w-full bg-primary lg:px-20">
                <div className="mx-auto px-4">
                    <div className="sm:flex sm:flex-wrap sm:-mx-4 md:py-4">
                        <div className="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6">
                            <h5 className="text-xl font-bold mb-6">Features</h5>
                            <ul className="list-none footer-links">
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Cool stuff</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Random feature</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Team feature</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Stuff for developers</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Another one</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Last time</a>
                            </li>
                            </ul>
                        </div>
                        <div className="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6 mt-8 sm:mt-0">
                            <h5 className="text-xl font-bold mb-6">Resources</h5>
                            <ul className="list-none footer-links">
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Resource</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Resource name</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Another resource</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Final resource</a>
                            </li>
                            </ul>
                        </div>
                        <div className="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6 mt-8 md:mt-0">
                            <h5 className="text-xl font-bold mb-6">A Propos</h5>
                            <ul className="list-none footer-links">
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Team</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Locations</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Privacy</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Terms</a>
                            </li>
                            </ul>
                        </div>
                        <div className="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6 mt-8 md:mt-0">
                            <h5 className="text-xl font-bold mb-6">Aide</h5>
                            <ul className="list-none footer-links">
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Support</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Help Center</a>
                            </li>
                            <li className="mb-2">
                                <a href="#" className="border-b border-solid border-transparent hover:border-pink-800 hover:text-pink-800">Contact Us</a>
                            </li>
                            </ul>
                        </div>
                        <div className="px-4 mt-4 sm:w-1/3 xl:w-2/6 sm:mx-auto xl:mt-0 xl:ml-auto justify-end flex">
                            <div>
                                <h5 className="text-xl font-bold mb-6 sm:text-center xl:text-left">Restez connecté</h5>
                                <div className="flex sm:justify-center xl:justify-start">
                                <a href="" className="w-8 h-8 border border-2 border-gray-400 rounded-full text-center py-1 text-gray-600 hover:text-white hover:bg-blue-600 hover:border-blue-600">
                                    <FaFacebook className='m-auto' color='white' />
                                </a>
                                <a href="" className="w-8 h-8 border border-2 border-gray-400 rounded-full text-center py-1 ml-2 text-gray-600 hover:text-white hover:bg-blue-400 hover:border-blue-400">
                                    <FaTwitter className='m-auto' color='white' />
                                </a>
                                <a href="" className="w-8 h-8 border border-2 border-gray-400 rounded-full text-center py-1 ml-2 text-gray-600 hover:text-white hover:bg-red-600 hover:border-red-600">
                                    <FaYoutube className='m-auto' color='white' />
                                </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="sm:flex sm:flex-wrap sm:-mx-4 mt-6 pt-6 sm:mt-12 sm:pt-12 border-t">
                        <div className="sm:w-full px-4 md:w-2/6">
                            <strong className='text-xl'>Dossier Medical</strong>
                        </div>
                        <div className="px-4 sm:w-1/2 md:w-1/4 mt-4 md:mt-0">
                            <h6 className="font-bold mb-2">Addresse</h6>
                            <address className="not-italic mb-4 text-sm">
                            123 6th St.<br />
                            Chateau Ngoaekele, FL 32904
                            </address>
                        </div>
                        <div className="px-4 md:w-1/4 md:ml-auto mt-6 sm:mt-4 md:mt-0">
                            <button className="px-4 py-2 bg-pink-700 hover:bg-pink-800 rounded text-white">Se connecter</button>
                        </div>
                    </div>
                </div>
            </footer>
    )
}

export default Footer