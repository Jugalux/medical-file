import React, { useState, useEffect, useContext } from "react";
import axios from "../../api/axios";
import { Modal } from "@mui/material";
import { BsPlusLg } from "react-icons/bs";
import AddInfoModal from "./AddInfoModal";
import { MedicalFolderContext } from "../../../services/MedicalFolder";

const OrdonnancesBox = ({ data, isPatient, patientId }) => {
  const {postOrdonnances} = useContext(MedicalFolderContext);
  const [ordonnancesList, setOrdonnancesList] = useState(data);
  const [disabled, setDisabled] = useState(true);
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    setOrdonnancesList(data)
  },[data])

  const handleClose = () => setOpenModal(false);
  const handleOpen = () => setOpenModal(true);

  const addData =async (content) => {
   postOrdonnances(content,patientId);
   setDisabled(true);
    handleClose();
  };

  return (
    <div className="relative bg-white  min-h-[12vh] md:min-h-[20vh] border rounded-md md:py-4 flex flex-col justify-center">
      <div className="flex  flex-wrap absolute left-[20%] top-[-20px] md:left-0">
        <div className=" flex justify-between items-center bg-secondary border rounded-full text-primary  text-sm md:text-xl py-1 md:py-2 px-3 md:px-5 ">
          <p>Ordonnance</p>
          <div
            className={` bg-primary text-secondary text-sm border rounded-full p-1 ml-2 hover:cursor-pointer ${
              (!disabled || isPatient) && "hidden"
            }`}
            onClick={() => {
              handleOpen();
              setDisabled(false);
            }}
          >
            <BsPlusLg />
          </div>
        </div>
      </div>

      <div className={`overflow-y-auto ${ordonnancesList.length === 0 ? "h-[18vh] flex items-center justify-center"  : "h-[58vh]"}`}>
        {" "}
        <div className="my-5 mx-5">
          {ordonnancesList.map((elt, id) => {
            return (
              <div key={id} className="bg-slate-100 p-3 my-3 border rounded-md">
                <p className="text-sm md:text-xl">{elt.ordonnance}</p>

                <div className="flex justify-between pt-4">
                  <small className='text-green-700 italic'>Edité par {elt.prescrite_par.utilisateur.first_name}</small>
                  <p>{elt.date.slice(0,10)} - {elt.date.slice(11,16)} </p>
                </div>
              </div>
            );
          })}

          {ordonnancesList.length === 0 && (
            <div className="text-center font-bold text-md md:text-2xl text-slate-400">
              <p>Aucune ordonnance pour le moment</p>
            </div>
          )}
        </div>
        
      </div>
      <div className="min-h-full">
        <Modal
          open={openModal}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <div className="w-full ">
            <AddInfoModal
              title="ordonnance"
              close={() => {
                handleClose();
                setDisabled(true);
              }}
              handleClick={addData}
            />
          </div>
        </Modal>
      </div>
    </div>
  );
};

export default OrdonnancesBox;
