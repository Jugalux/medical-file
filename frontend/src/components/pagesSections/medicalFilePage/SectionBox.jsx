import React from "react";
import ExamsBox from "./ExamsBox";
import OrdonnancesBox from "./OrdonnancesBox";
import PersonalInformationBox from "./PersonalInformationBox";
import ObservationsBox from "./ObservationsBox";

const SectionBox = ({ data, observations, exams, ordonnances, isPatient, patientId }) => {
  return (
    <section className="py-10 md:py-7 [&>*]:mb-10">
      <PersonalInformationBox data={data} isPatient={isPatient} patientId = {patientId}/>
      <ObservationsBox data={observations}  isPatient={isPatient} patientId = {patientId}/>
      <div className="lg:flex ">
        <div className="lg:basis-1/2 lg:mr-2 mb-10 lg:mb-none">
          <ExamsBox data={exams} isPatient={isPatient} patientId = {patientId}/>
        </div>
        <div className=" lg:basis-1/2 lg:ml-2">
          <OrdonnancesBox data={ordonnances} isPatient={isPatient} patientId = {patientId}/>
        </div>
      </div>
    </section>
  );
};

export default SectionBox;
