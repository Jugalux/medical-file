import React, { useState } from "react";
import {ImCross} from "react-icons/im";
import { AiFillSave } from "react-icons/ai";
import Button from "../../elements/Button";

const AddInfoModal = ({ title, close , handleClick}) => {
  const [value, setValue] = useState("");
  return (
    <section className="bg-transparent  h-[100vh] flex justify-center items-center ">
      
      <div className="bg-white px-5 py-3 border rounded-md min-w-[85vw] md:min-w-[40vw] relative">
      <ImCross
          className="text-primary absolute right-3 top-3 hover:cursor-pointer hover:text-blue-800"
          onClick={close}
        />
        <p className="font-bold text-xl mb-2 text-center">
          Ajouter une {title}
        </p>
        <div className="my-3">
          <textarea
            className="peer w-full px-2 py-2 border border-slate-300 rounded-md text-[18px] shadow-sm placeholder-slate-400 focus:outline-none focus:border-primary focus:border-2"
            value={value}
            onChange={(e) => setValue(e.target.value)}
          />
        </div>
        <div
          className="flex justify-center items-center [&>*]:mx-2"
        >
          <Button
            name="Annuler"
            filled={false}
            color={"#FF0000"}
            handleClick={() => close()}
          >
            <ImCross />
          </Button>
          <Button
            name="Enregistrer"
            filled={true}
            handleClick={() => handleClick(value)}
          >
            <AiFillSave />
          </Button>
        </div>
      </div>
    </section>
  );
};

export default AddInfoModal;