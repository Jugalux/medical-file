import React, { useState } from "react";
import axios from "../api/axios";
import { ImCross } from "react-icons/im";
import Title from "./../elements/Title";
import Button from "./../elements/Button";
import PersonalInfoInput from "./PersonalInfoInput";

const sexeEnum = ["M", "F"];
const statutEnum = ["Publique", "Privé"];

const CreateMedicalFilePopup = ({ close }) => {
  const [formInputs, setFormInputs] = useState({
    nom: "",
    prenom: "",
    date: "",
    lieu: "",
    taille: "",
    poids: "",
    sexe: sexeEnum[0],
    profession: "",
    cni: "",
    tel: "",
    statut: statutEnum[0],
  });

  const handleChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    setFormInputs({ ...formInputs, [name]: value });
  };

  const postMedicalFolder = async () => {
    if (
      !formInputs.nom ||
      !formInputs.prenom ||
      !formInputs.date ||
      !formInputs.lieu ||
      !formInputs.poids ||
      !formInputs.taille ||
      !formInputs.sexe
    ) {
      alert("un ou plusieurs champs manquants");
    } else {
      const postData = {
        nom: formInputs.nom,
        prenom: formInputs.prenom,
        date_de_naissance: formInputs.date,
        lieu_de_naissance: formInputs.lieu,
        poids: parseInt(formInputs.poids),
        taille: parseFloat(formInputs.taille),
        sexe: formInputs.sexe,
        profession: formInputs.profession ? formInputs.profession : "",
        numero_cni: formInputs.cni ? formInputs.cni : null,
        telephone: formInputs.tel ? formInputs.tel : "",
        status: formInputs.statut ? formInputs.statut : "",
        cree_par: window.localStorage.getItem('userId'),
      };
      const res = await axios.post("/medical_folder/", postData, {
        headers: { "Content-Type": "application/json" },
      });
      console.log("enter");
      console.log(res);
      close()
    }
  };

  return (
    <section className="bg-transparent w-full min-h-full flex justify-center items-center ">
      <div className="lg:w-[50vw] md:w-[70vw] w-[100vw] bg-white py-4 px-5 relative m-4 rounded-sm ">
        <ImCross
          className="text-primary absolute right-3 top-3 hover:cursor-pointer hover:text-blue-800"
          onClick={() => close()}
        />
        <Title
          titleName="Ajout d'un nouveau dossier"
          subtitleName="Créez un nouveau dossier patient en remplissant les champs suivants"
        />
        <PersonalInfoInput
          itemsPerLine={2}
          modify={false}
          disabled={false}
          formInputs={formInputs}
          handleChange={handleChange}
        />
        <div className="my-2">
          <Button
            name="Créer un nouveau compte patient"
            filled={true}
            handleClick={postMedicalFolder}
          />
        </div>
      </div>
    </section>
  );
};

export default CreateMedicalFilePopup;
