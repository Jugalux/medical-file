import axios from 'axios';

export default axios.create({
    baseURL: `https://medical-file-api.onrender.com/medical-folder/api/`
})
