import React from "react";

const Button = ({ name, filled, children, handleClick, color }) => {
  return (
    <div
      className={` ${
        filled
          ? " hover:bg-blue-800"
          : " hover:bg-blue-100"
      } hover:cursor-pointer border rounded-full py-2 px-2 flex justify-center items-center text-center`}
      style={{
        borderColor: color ? color : "#003570",
        backgroundColor: filled ? (color ? color : "#003570") : "transparent",
        color: !filled ? (color ? color : "#003570"):"white"
      }}
      onClick={() => handleClick()}
    >
      {children}
      <p className="text-sm md:text-xl ml-2">{name}</p>
    </div>
  );
};

export default Button;
