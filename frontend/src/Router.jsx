import React from 'react'
import { Routes, Route } from "react-router-dom"

import Home from '../src/pages/home/Home'
import MedicalFolder from '../src/pages/medicalFolder/MedicalFolder'
import Search from '../src/pages/search/Search'
import NotFound from '../src/pages/notFound/NotFound'
import Connexion from '../src/pages/connexion/connexion'

const Router = () => {

	return(
		<section className="bg-projectBackground min-h-[100vh] w-full bg-cover bg-no-repeat">
			<Routes>
			<Route path="/" >

				<Route index element={<Home />} />
				<Route path="search" element={<Search  />} />
				<Route path="login" element={<Connexion  />} />
				<Route path="file/:id" element = {<MedicalFolder/>} />
				<Route path="*" element={ <NotFound /> } />

			</Route>
		</Routes>
		</section>
	)
}

export default Router;