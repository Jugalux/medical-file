import React, { useState, useEffect } from "react"
import axios from '../../../components/api/axios'
import { useNavigate } from "react-router"
import Avatar from "../../../media/avatar.png"
import { ImCross } from "react-icons/im"
import { FiEye, FiEyeOff } from "react-icons/fi"

const ContainerConnexion = () => {

	const [err, setError] = useState(null)
	const [passShow, setPassShow] = useState('password')
	const [username, setUsername] = useState(null);
	const [password, setPassword] = useState(null);
	const [loading, setLoading] = useState(null);

	let navigate = useNavigate()

	const handleSubmit = (e) => {
		e.preventDefault()
		setLoading('...')
		axios.post('/doctor/connection/', {
			username,
			password
		}).then(res => {
			console.log(res, 'ressssssss')
			setError(res?.data)				
			navigate('/search');
			if (res?.data !== "L'adresse mail n'existe pas.") {
				window.localStorage.setItem('data', JSON.stringify(res?.data))
				window.localStorage.setItem('userId', JSON.stringify(res?.data?.id))
				//navigate('/search');
			}
		}).catch(err => {
			console.log(err, 'erererer')
			setError(err?.data?.username[0], err?.data)
		}).finally(
			console.log('endedded')
		)
	}

	return (
		<>
			<div className="bg-blue-900 text-center">
				{err && <p className="p-3 text-white w-9/12">{err} <ImCross
					className="text-red-500 absolute right-3 top-3 hover:cursor-pointer hover:text-white"
					onClick={() => setError(null)}
				/></p>} {console.log(window.localStorage.getItem('data'), 'dtqtqtqt')}
			</div>
			<div className="flex h-screen w-full items-center justify-center bg-gray-900 bg-cover bg-no-repeat bg-[url('https://images.pexels.com/photos/40568/medical-appointment-doctor-healthcare-40568.jpeg?auto=compress&cs=tinysrgb&w=1600')]" >
				<div className="rounded-xl bg-slate-200 bg-opacity-50 px-16 py-10 shadow-lg backdrop-blur-md max-sm:px-8 md:w-5/12 w-9/12">
					<div className="text-white">
						<div className="mb-8 flex flex-col items-center">
							<img src={Avatar} width="150" alt="" />
							<h1 className="mb-2 text-2xl text-blue-900 font-medium">Welcome !</h1>
							<span className="text-blue-900">Login to continue.</span>
						</div>
						<form action="#" onSubmit={handleSubmit}>
							<div className="mb-4 text-lg">
								<label htmlFor="" className="text-sm text-blue-800">Entrez votre email</label> <br />
								<input 
									onChange={(e)=>{
										setUsername(e.target.value)
									}}
									value={username}
									className="rounded-lg text-sm text-black border focus:border-2 focus:border-blue-900 bg-white bg-opacity-50 px-6 py-3 w-full outline-none backdrop-blur-md"
									type="email"
									name="name"
									placeholder="id@email.com" />
							</div>

							<div className="mb-4 text-lg">
								<label htmlFor="" className="text-sm text-blue-800">Entrez votre mot de passe</label> <br />
								<div className="flex justify-around items-center">
								<input 
									onChange={(e)=>{
										setPassword(e.target.value)
									}}
									value={password}
									className="rounded-lg text-sm text-black border focus:border-2 focus:border-blue-900 bg-white bg-opacity-50 px-6 py-3 w-full outline-none backdrop-blur-md -ml-3"
									type={passShow}
									name="name"
									placeholder="kjhzk65-0h" />
									<p
										className="cursor-pointer text-sm text-blue-900 -mr-12"
										onClick={() => {
											passShow === 'password' ? setPassShow('text') : setPassShow('password')
										}}>
										{passShow === 'password' ? <span><FiEyeOff size={15} /></span> : <span><FiEye size={15} /></span>}</p>
								</div>
							</div>
							<div className="mt-8 flex justify-center text-lg text-black">
								<button type="submit" className="rounded-lg bg-blue-900 px-10 py-2 text-white shadow-xl backdrop-blur-md transition-colors duration-300 hover:bg-red-600/75">Login {loading} </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</>
	)
}

export default ContainerConnexion;
