import React from 'react'
import Navbar from '../../components/pagesSections/Navbar'
import Footer from '../../components/pagesSections/Footer'
import ContainerHome from './container/ContainerHome'

function Home() {

    return (
        <div className=''>
            <Navbar page='home' />
            <ContainerHome />
            <Footer />
        </div>
    )
}

export default Home