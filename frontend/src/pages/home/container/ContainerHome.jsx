import React, { useState } from 'react'
import { AiFillFolder } from 'react-icons/ai'
import { Modal } from "@mui/material"
import Diaporama from '../../../components/pagesSections/homePage/Diaporama'
import ModalAddFile from '../../../components/pagesSections/ModalAddFile'
import banner from '../../../media/banner.png'
import Services from '../../../components/pagesSections/homePage/Services'
import About from '../../../components/pagesSections/homePage/About'


function ContainerHome() {

    const [openModal, setOpenModal] = useState(false)
    const handleClose = () => setOpenModal(false)
	const handleOpen = () => setOpenModal(true)

    return (
        <div>
            <div className='grid lg:grid-cols-5 p-10 lg:pb-0 pb-15 lg:px-20'>
                <div className='lg:col-span-3 lg:pt-16'>
                    <h1 className='text-4xl font-semibold my-5 text-primary'>
                        Bienvenue sur la plate forme <p className=''>Dossier Médical</p>
                    </h1>
                    <p className='text-gray-800'>
                        La plateforme qui permet à tout citoyen Camerounais d'accéder à son dossier médical de n'importe ou,
                        et pour tout médecin de créer et d'accéder aux différents dossiers de ses patients.
                        Ici, toutes vos données sont privées et confidentielles... <br />
                        Faites nous confiance et nous nous occupons du reste !
                    </p>
                    <div className='flex mt-12'>
                        <div onClick={() => handleOpen()} className="flex lg:px-10 px-4 lg:py-3 py-2 bg-danger hover:bg-red-500 transition text-white border rounded-full hover:cursor-pointer">
                            <AiFillFolder color='white' size={30} className='mr-2' />
                            <p className='text-lg'>Accéder à mon dossier</p>
                        </div>
                        <Modal
                            open={openModal}
                            onClose={handleClose}
                            aria-labelledby="modal-modal-title"
                            aria-describedby="modal-modal-description"
                            sx={{ overflowY: "scroll" }}
                        >
                            <div className="w-full">
                                <ModalAddFile close={handleClose} />
                            </div>
                        </Modal>
                    </div>
                </div>
                <div className='lg:col-span-2 lg:pt-0 pt-16'>
                    <img src={banner} alt="" />
                </div>
            </div>

            <Diaporama />
            <Services />
            <About />
        </div>
    )
}

export default ContainerHome