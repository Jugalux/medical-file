import React, { useEffect, useState , useContext} from "react";
import * as Axios from "axios";
import axios from "../../../components/api/axios";
import { FaDownload } from "react-icons/fa";
import {  useParams } from "react-router-dom";
import Button from "../../../components/elements/Button";
import SectionBox from "../../../components/pagesSections/medicalFilePage/SectionBox";
import { MedicalFolderContext } from "../../../services/MedicalFolder";


const ContainerMedicalFolder = () => {
  const {patientInfo,observationInfo,examsInfo, ordonnancesInfo, requestData, patientId  } = useContext(MedicalFolderContext);
  const [isPatient, setIsPatient] = useState(false);

  const params = useParams();

  const handleDownload = () => {
    console.log("donwload");
  };

  useEffect(() => {
    requestData(params);
  }, []);

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has("id")) {
      setIsPatient(true)
    }
  }, [])

  return (
    <div className="min-h-[100vh] max-w-full p-5">
      {/** bouton télécharger */}
      <div className=" flex justify-end">
        <div className="w-[130px] md:w-[160px] flex-wrap">
          <Button
            filled={false}
            name="Télécharger"
            handleClick={handleDownload}
          >
            <FaDownload />
          </Button>
        </div>
      </div>

      <div className='my-5 flex justify-center'>
        <p className='text-center bg-danger text-white px-10 py-3 rounded-full'>
          Code patient: <span className='font-bold'>{patientInfo.code}</span>
        </p>
      </div>

      <SectionBox
        data={patientInfo}
        observations={observationInfo}
        exams={examsInfo}
        ordonnances={ordonnancesInfo}
        isPatient={isPatient}
        patientId = {patientId}
      />
    </div>
  );
};

export default ContainerMedicalFolder;
