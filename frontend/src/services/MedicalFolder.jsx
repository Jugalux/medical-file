import React, { createContext, useState, useEffect } from "react";
import axios from "../components/api/axios";
import * as Axios from "axios";

export const MedicalFolderContext = createContext();

export const MedicalFolderContextProvider = ({ children }) => {
  const [patientInfo, setPatientInfo] = useState([]);
  const [observationInfo , setObservationInfo] = useState([]);
  const [examsInfo ,setExamsInfo ] = useState([]);
  const [ordonnancesInfo ,setOrdonnancesInfo ] = useState([]);
  const [patientId , setPatientId] = useState(null);


  const requestData = async (params) => {
    if (params.id) {
      const res = await axios.get("/medical_folder/");
      const paramId = parseInt(params.id);
      setPatientId(paramId);
      const findInfo = res.data.find((elt) => elt.id === paramId);
      if (findInfo) {
        
        setPatientInfo(findInfo);
        Axios.all([
          axios.get(`/ordonnances/`), 
          axios.get(`/examens/`) ,
          axios.get(`/observations/`)
        ])
        .then(Axios.spread((data1, data2, data3) => {
          console.log("data 1 is ", data1.data , " data 2 is ", data2.data , " data3 is ", data3.data);
          const patientOrdonnance = data1.data.filter((elt) => elt.dossier_medical === paramId);
          const patientExam = data2.data.filter((elt) => elt.dossier_medical === paramId);
          const patientObservation = data3.data.filter((elt) => elt.dossier_medical === paramId);
          // output of req.
          if(data1.status === 200){setOrdonnancesInfo(patientOrdonnance)}
          if(data2.status === 200){setExamsInfo(patientExam)}
          if(data3.status === 200){setObservationInfo(patientObservation)}
        }))
      }
    }
  };

  const getOrdonnances = async (patientId) => {
    const res = await axios.get("/ordonnances/");
    setOrdonnancesInfo(res.data.filter((elt) => elt.dossier_medical === patientId))

  }

  const getObservations = async (patientId) => {
    const res = await axios.get("/observations/");
    setObservationInfo(res.data.filter((elt) => elt.dossier_medical === patientId))

  }


  const getExams = async (patientId) => {
    const res = await axios.get("/examens/");
    setExamsInfo(res.data.filter((elt) => elt.dossier_medical === patientId))

  }

  const postOrdonnances = async (content,patientId) => {
    const res = await axios.post("/ordonnances/", {
        ordonnance: content,
        dossier_medical: patientId,
        prescrite_par: window.localStorage.getItem('userId'),
      });
      console.log("post ordonnances res",res);
      getOrdonnances(patientId)
  }

  const postObservations = async (content,patientId) => {
    const res = await axios.post("/observations/", {
        observation: content,
        dossier_medical: patientId,
        redigee_par: window.localStorage.getItem('userId'),
      });
      console.log("post ordonnances res",res);
      getObservations(patientId)
  }

  const postExams = async (content,patientId) => {
    const res = await axios.post("/examens/", {
        examen: content,
        dossier_medical: patientId,
        demande_par: window.localStorage.getItem('userId'),
      });
      console.log("post ordonnances res",res);
      getExams(patientId)
  }

  return (
    <MedicalFolderContext.Provider
      value={{
        requestData, 
        patientInfo,
        observationInfo,
        examsInfo,
        ordonnancesInfo,
        patientId,
        postOrdonnances,
        postExams,
        postObservations
      }}
    >
      {children}
    </MedicalFolderContext.Provider>
  );
};
